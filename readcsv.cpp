#include "readcsv.h"
#include "ui_readcsv.h"
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QDebug>
#include "globals.h"

QRegularExpression sep_vars("\\s*,\\s*|\\s*;\\s*|\\s+");
QRegularExpression sep_lines("\u2029|\\r\\n|\\r|\\n");
QString oneline;
QStringList lines;


Readcsv::Readcsv(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Readcsv)
{
    ui->setupUi(this);


    QFile inputFile(fileName);
    QVector<double> x, y;

    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        oneline = in.readAll();
        oneline = oneline.trimmed();
        lines = oneline.split(sep_lines);

        QString line = lines[0];
        line = line.trimmed();
        QStringList list = line.split(sep_vars);

        bool ok = true;
                //str_ok = true; // first line are numbers
        double fl;


        foreach(QString str, list){
            fl = str.toDouble(&ok);
            if (!ok) str_ok = false;
        }

        QStringList headerLabels;

        for (int i=0; i<list.length(); ++i){
            if (str_ok)
            {ui->comboBox->addItem(QString::number(i+1));
                ui->comboBox_2->addItem(QString::number(i+1));
            headerLabels << QString::number(i+1);}
            else {
                ui->comboBox->addItem(list[i]);
                ui->comboBox_2->addItem(list[i]);
                headerLabels << list[i];
            }
        }

        ui->comboBox->setCurrentIndex(0);
        ui->comboBox_2->setCurrentIndex(1);

        ui->tableWidget->setColumnCount(list.length());
        ui->tableWidget->setHorizontalHeaderLabels(headerLabels);


        if(!str_ok) lines.removeFirst();

        for(int i = 0; i<lines.length();i++)
        {
            line = lines[i];
            line = line.trimmed();
            list = line.split(sep_vars);
            ui->tableWidget->insertRow(i);
            for(int j=0; j<list.length();j++) ui->tableWidget->setItem(i,j,new QTableWidgetItem(list[j]));
         }
        inputFile.close();
    }
}

Readcsv::~Readcsv()
{
    delete ui;
}


void Readcsv::on_buttonBox_accepted()
{
    QVector<double> x, y;
    QString line;
    QStringList list;
    bool ok;

       for(int i=0;i<lines.length();i++)
           {
              line = lines[i];
              line = line.trimmed();
              list = line.split(sep_vars);

              x.append(list.at(ui->comboBox->currentIndex()).toDouble(&ok));
              if (!ok) {QMessageBox msgBox;
                  msgBox.setText("Invalid number format, line: "+QString::number(i+1));
                  msgBox.exec();
                  return;}
              y.append(list.at(ui->comboBox_2->currentIndex()).toDouble(&ok));
              if (!ok) {QMessageBox msgBox;
                  msgBox.setText("Invalid number format, line: "+QString::number(i+1));
                  msgBox.exec();
                  return;}
              if (i>0) if (x[i]<=x[i-1]) {QMessageBox msgBox;
                  msgBox.setText("x must be in increasing order!");
                  msgBox.exec();
                  return;}
           }




    if (fType == "d") {
        x_d.clear();y_d.clear();x_d1.clear();y_dn.clear();
        x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;

        x_d = x;
        y_d = y;
        n_d=y_d.length();
        x_d1=x_d;
        data_orig.x=x_d;
        data_orig.y=y_d;


    }
    if (fType == "t") {
        x_t.clear();y_t.clear();y_tn.clear();
        x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;
        x_t = x;
        y_t = y;
        n_t=y_t.length();
        target_orig.x=x_t;
        target_orig.y=y_t;
    }
    if (fType == "tm") {
        tm_orig.x=x;
        tm_orig.y=y;
    }

    }
