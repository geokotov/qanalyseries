#ifndef ETP_H
#define ETP_H

#include <QDialog>

namespace Ui {
class ETP;
}

class ETP : public QDialog
{
    Q_OBJECT

public:
    explicit ETP(QWidget *parent = nullptr);
    ~ETP();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::ETP *ui;
};

#endif // ETP_H
