#ifndef GLOBALS_H
#define GLOBALS_H
#include<QVector>

extern QVector<double> x_d, y_d, x_d1, x_t, y_t, x_tm, y_tm, y_dn, y_tn; //dn and tn - for normalized graphs;
extern QVector<int> xind_tm, yind_tm, xind_tm_orig, yind_tm_orig;
extern int n_d, n_t, n_tm, ind0, ind1, ind2, screenw, screenh;
extern double psd_min, psd_max;
extern QString fileName,fType;

struct DataStr
{
    QVector<double> x;
    QVector<double> y;

} ;
extern struct DataStr data_orig;
extern struct DataStr target_orig;
extern struct DataStr tm_orig;

struct Orbit
{
    double time;
    double ecc;
    double eps;
    double varpi;
};

#endif // GLOBALS_H
