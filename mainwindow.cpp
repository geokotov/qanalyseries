#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "globals.h"
#include "qcustomplot.h"
#include <QDir>
#include <QDebug>
#include "correlation.h"
#include "sedrate.h"
#include "tuning.h"
#include <QFileDialog>
#include <QKeyEvent>
#include <fstream>
#include "mynr.h"
#include "laskar.h"
#include "dataproc.h"
#include "ssa.h"
#include "QTextStream"
#include "etp.h"
#include "insolation.h"
#include <iostream>
#include "readcsv.h"

static QVector<QCPItemLine*> arrows; // initiates an array of pointers on arrows between data and target tie points
static QVector<QCPItemText*> depths; // initiates an array of pointers on depth text labels of tuned data
static QString fName = "untitled.txt", prName = "untitled.prj";
static int command_line;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //QDir::setCurrent(QCoreApplication::applicationDirPath());
    QDir::setCurrent(QDir::homePath());

    n_tm=0; n_d = 0; n_t = 0;
    x_tm.clear(); y_tm.clear();
    tm_orig.x.clear();tm_orig.y.clear();

    //    QScreen *screen = QGuiApplication::primaryScreen();
    //    QRect  screenGeometry = screen->geometry();
    //    screenh = screenGeometry.height();
    //    screenw = screenGeometry.width();


    QSize availableSize = qApp->desktop()->availableGeometry().size();
    screenw = availableSize.width();
    screenh = availableSize.height();

    MainWindow::setGeometry(0,screenh*0.05,screenw*0.15,screenh*0.95);

    QList<QCPAxis *> axlist;
    correlation = new Correlation;
    axlist << correlation->corrGraphic->xAxis <<  correlation->corrGraphic->xAxis2 ;
    correlation->corrGraphic->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    correlation->corrGraphic->axisRect()->setRangeDragAxes(axlist);
    correlation->corrGraphic->axisRect()->setRangeZoomAxes(axlist);
    correlation->corrGraphic->addGraph(correlation->corrGraphic->xAxis2, correlation->corrGraphic->yAxis2);
    correlation->corrGraphic->xAxis2->setLabel("Data, x");
    correlation->corrGraphic->yAxis2->setLabel("Data, y");
    correlation->corrGraphic->graph(0)->rescaleAxes();
    correlation->corrGraphic->graph(0)->setName("Data");
    correlation->corrGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 5));
    correlation->corrGraphic->graph(0)->setSelectable(QCP::stSingleData);
    correlation->corrGraphic->xAxis2->setVisible(true);
    correlation->corrGraphic->yAxis2->setVisible(true);
    correlation->corrGraphic->legend->setVisible(true);
    correlation->corrGraphic->addGraph(correlation->corrGraphic->xAxis, correlation->corrGraphic->yAxis);
    correlation->corrGraphic->graph(1)->setPen(QPen(Qt::red));
    correlation->corrGraphic->xAxis->setLabel("Target, x");
    correlation->corrGraphic->yAxis->setLabel("Target, y");
    correlation->corrGraphic->graph(1)->rescaleAxes();
    correlation->corrGraphic->graph(1)->setName("Target");
    correlation->corrGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 5));
    correlation->corrGraphic->graph(1)->setSelectable(QCP::stSingleData);
    correlation->show();
    axlist.clear();

    sedrate = new Sedrate;
    axlist << sedrate->sedGraphic->xAxis;
    sedrate->sedGraphic->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    sedrate->sedGraphic->axisRect()->setRangeDragAxes(axlist);
    sedrate->sedGraphic->axisRect()->setRangeZoomAxes(axlist);
    sedrate->sedGraphic->addGraph(sedrate->sedGraphic->xAxis, sedrate->sedGraphic->yAxis);
    sedrate->sedGraphic->xAxis->setLabel("Target, x");
    sedrate->sedGraphic->yAxis->setLabel("Sed. rate");
    sedrate->sedGraphic->graph(0)->rescaleAxes();
    sedrate->sedGraphic->graph(0)->setName("Sedimentation rate");
    sedrate->sedGraphic->xAxis->setVisible(true);
    sedrate->show();
    axlist.clear();

    tuning = new Tuning;
    axlist << tuning->tuneGraphic->xAxis <<  tuning->tuneGraphic->xAxis2 ;
    tuning->tuneGraphic->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables | QCP::iSelectItems);
    tuning->tuneGraphic->axisRect()->setRangeDragAxes(axlist);
    tuning->tuneGraphic->axisRect()->setRangeZoomAxes(axlist);
    tuning->tuneGraphic->addGraph(tuning->tuneGraphic->xAxis2, tuning->tuneGraphic->yAxis2);
    tuning->tuneGraphic->xAxis2->setLabel("Data, x");


    tuning->tuneGraphic->yAxis2->setRange(-1.2, 1.2);
    tuning->tuneGraphic->graph(0)->setName("Data");
    tuning->tuneGraphic->graph(0)->setPen(QPen(Qt::blue));
    tuning->tuneGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    tuning->tuneGraphic->graph(0)->setSelectable(QCP::stSingleData);
    tuning->tuneGraphic->xAxis2->setVisible(true);
    tuning->tuneGraphic->yAxis2->setVisible(false);
    tuning->tuneGraphic->yAxis->setVisible(false);
    tuning->tuneGraphic->legend->setVisible(true);
    tuning->tuneGraphic->addGraph(tuning->tuneGraphic->xAxis, tuning->tuneGraphic->yAxis);
    tuning->tuneGraphic->graph(1)->setPen(QPen(Qt::red));
    tuning->tuneGraphic->xAxis->setLabel("Target, x");
    //tuning->tuneGraphic->yAxis->setLabel("Target, y");
    tuning->tuneGraphic->yAxis->setRange(-1.2, 1.2);
    tuning->tuneGraphic->graph(1)->setName("Target");
    tuning->tuneGraphic->graph(1)->setPen(QPen(Qt::red));
    tuning->tuneGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    tuning->tuneGraphic->graph(1)->setSelectable(QCP::stSingleData);
    //tuning->tuneGraphic->graph(0)->selectionDecorator()->setPen(QPen(Qt::black,4));
    //tuning->tuneGraphic->graph(1)->selectionDecorator()->setPen(QPen(Qt::black,4));

    // Selection markers
    tuning->tuneGraphic->addGraph(tuning->tuneGraphic->xAxis2, tuning->tuneGraphic->yAxis2);
    tuning->tuneGraphic->graph(2)->setPen(QPen(Qt::black));
    tuning->tuneGraphic->graph(2)->setLineStyle(QCPGraph::lsNone);
    tuning->tuneGraphic->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCrossCircle, 10));
    tuning->tuneGraphic->addGraph(tuning->tuneGraphic->xAxis, tuning->tuneGraphic->yAxis);
    tuning->tuneGraphic->graph(3)->setPen(QPen(Qt::black));
    tuning->tuneGraphic->graph(3)->setLineStyle(QCPGraph::lsNone);
    tuning->tuneGraphic->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCrossCircle, 10));
    tuning->tuneGraphic->legend->setVisible(false);

    tuning->show();

    connect(tuning->tuneGraphic, &QCustomPlot::selectionChangedByUser, this, &MainWindow::mousePress);
    connect(tuning->tuneGraphic, &QCustomPlot::mouseWheel, this, &MainWindow::mouseWheel);
    connect(tuning, &Tuning::myDropSignal, this, &MainWindow::replotAll);
    connect(sedrate, &Sedrate::myDropSignal, this, &MainWindow::onSedrateDrop);

    // ******** command line start with parameters, read me, Heiko!!! ***********
    // "open QAnalySeries.app --args -d datafile -t targetfile -tm timemodelfile" or
    // "open QAnalySeries.app --args -pr projectfile"
    // Possible slots for the output:
    //    (x_d, y_d; x_t, y_t) - data and target x-y values (e.g after pre-processing)
    //    (x_d1, y_d) - ages and values for data
    //    (x_tm, y_tm) x-y values for the time model (e.g. depth-age tie points)


    QStringList sl=QApplication::arguments();
    command_line = 0;
    if (sl.length() > 1) {
        command_line = 1;
        for (int i=1; i<sl.length()-1; i+=2) {
            if (sl[i] == "-d") {fileName = sl[i+1]; on_actionOpen_Data_triggered();}
            if (sl[i] == "-t") {fileName = sl[i+1]; on_actionOpen_Target_triggered();}
            if (sl[i] == "-tm") {fileName = sl[i+1]; on_actionOpen_TimeModel_triggered();}
            if (sl[i] == "-pr") {fileName = sl[i+1]; on_actionOpen_project_triggered();}
        }
        command_line = 0;
    }
    // ********Stop command line start***********************************************

}



MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_clicked()
{

tuning->show();
}

void MainWindow::on_actionOpen_Data_triggered()
{
    if (command_line == 0) {
        fileName = QFileDialog::getOpenFileName(this);


        QFile file(fileName);
        if(!file.exists()){
            qDebug() << "File does not exist: "<<fileName;return;
        }else{
            qDebug() << fileName<<" opened";
        }
    }
    fType = "d";
    readFile();

    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows
    replotAll();
}

void MainWindow::on_actionOpen_Target_triggered()
{   if (command_line == 0) {
        fileName = QFileDialog::getOpenFileName(this);

        QFile file(fileName);
        if(!file.exists()){
            qDebug() << "File does not exist: "<<fileName;return;
        } else {
            qDebug() << fileName<<" opened";
        }
    }
    fType = "t";
    readFile();

    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows

    replotAll();
}

void MainWindow::on_actionOpen_TimeModel_triggered()
{        if (n_d<2 || n_t<2) {QMessageBox msgBox;
        msgBox.setText("Download Data and Target first!");
        msgBox.exec();
        return;}
    if (command_line == 0) {
        fileName = QFileDialog::getOpenFileName(this);


        QFile file(fileName);
        if(!file.exists()){
            qDebug() << "File does not exist: "<<fileName;return;
        }else{
            qDebug() << fileName<<" opened";
        }
    }

    fType = "tm";
    readFile();

    double x,y, interp;
    QVector<double> xtmp(2), ytmp(2);

            x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();
            int i=0;

            for (int j = 0; j<tm_orig.x.length();j++)
                    {
                       x = tm_orig.x[j]; y = tm_orig.y[j];

                        x_tm.append(x); y_tm.append(y);

                       i=0;
                       while (i<x_d.length() && x>x_d[i]) i++; xind_tm.append(i); //

                       if (i>(x_d.length()-1)) {xtmp[0]=x_d[i-2]; xtmp[1]=x_d[i-1];
                                                ytmp[0]=y_d[i-2]; ytmp[1]=y_d[i-1];}
                       else if (i==0  && x!=x_d[i]) {xtmp[0]=x_d[0]; xtmp[1]=x_d[1];
                                                     ytmp[0]=y_d[0]; ytmp[1]=y_d[1];}
                       else if (i>0 && i<=(x_d.length()-1) && x!=x_d[i]) {xtmp[0]=x_d[i-1]; xtmp[1]=x_d[i];
                                                                         ytmp[0]=y_d[i-1]; ytmp[1]=y_d[i];}

                       if (i>(x_d.length()-1) || x!=x_d[i]) {
                          x_d.insert(i,x);
                          mynr::linterp(xtmp,ytmp,x_d[i],interp);
                          y_d.insert(i,interp);
                       }

                       i=0;
                       while (i<x_t.length() && y>x_t[i]) i++; yind_tm.append(i);

                       if (i>(x_t.length()-1)) {xtmp[0]=x_t[i-2]; xtmp[1]=x_t[i-1];
                                                ytmp[0]=y_t[i-2]; ytmp[1]=y_t[i-1];}


                       else if (i==0 && y!=x_t[i] ) {xtmp[0]=x_t[0]; xtmp[1]=x_t[1];
                                                     ytmp[0]=y_t[0]; ytmp[1]=y_t[1];}

                        else if (i>0 && i<=(x_t.length()-1) && y!=x_t[i]) {xtmp[0]=x_t[i-1]; xtmp[1]=x_t[i];
                                                                           ytmp[0]=y_t[i-1]; ytmp[1]=y_t[i];}

                       if (i>(x_t.length()-1) || y!=x_t[i]) {
                       x_t.insert(i,y);
                           mynr::linterp(xtmp,ytmp,x_t[i],interp);
                           y_t.insert(i,interp);}

                    }


            n_tm=x_tm.length();n_d=x_d.length(); n_t=x_t.length();

            y_dn=y_d;// normalized target to [-1 0];
            double minv, maxv;

            minv = *std::min_element(y_d.constBegin(), y_d.constEnd());
            maxv = *std::max_element(y_d.constBegin(), y_d.constEnd());

            for (int i=0; i<n_d; i++) {y_dn[i]=(y_d[i]-minv)/(maxv-minv)+0.1;}

            y_tn=y_t;// normalized target to [-1 0];

            minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
            maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());

            for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;}


            recalculateData();
            //replotAll();

            tuning->tuneGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
            tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
            tuning->tuneGraphic->replot();
            correlation->corrGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
            correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
            correlation->corrGraphic->replot();

            tm_orig.x = x_tm;
            tm_orig.y = y_tm;
            xind_tm_orig = xind_tm;
            yind_tm_orig = yind_tm;
            data_orig.x=x_d;
            data_orig.y=y_d;
            target_orig.x=x_t;
            target_orig.y=y_t;

}

void MainWindow::mousePress()
{
    QCPDataSelection selection0 = tuning->tuneGraphic->graph(0)->selection();

    if (!selection0.isEmpty()) {
        ind0 = selection0.dataRange().begin();
        tuning->tuneGraphic->xAxis2->setLabel(QString("Data, x: %1, y:  %2").arg(x_d[ind0]).arg(y_d[ind0]));
        tuning->tuneGraphic->graph(2)->data()->clear();
        tuning->tuneGraphic->graph(2)->addData(x_d1[ind0], y_dn[ind0]);

    }

    QCPDataSelection selection1 = tuning->tuneGraphic->graph(1)->selection();
    if (!selection1.isEmpty()){
        ind1 = selection1.dataRange().begin();
        tuning->tuneGraphic->xAxis->setLabel(QString("Target, x: %1, y:  %2").arg(x_t[ind1]).arg(y_t[ind1]));
        tuning->tuneGraphic->graph(3)->data()->clear();
        tuning->tuneGraphic->graph(3)->addData(x_t[ind1], y_tn[ind1]);

    }



    if(QApplication::keyboardModifiers() & Qt::ShiftModifier)  // recalculate data
    {
        int i = 0;
        while (i<n_tm && x_tm[i] < x_d[ind0]) ++i;
        if (i==n_tm || i==0  ||  ((x_t[ind1]-y_tm[i-1])*(x_t[ind1]-y_tm[i])<0.0 && (x_d[ind0]-x_tm[i-1])*(x_d[ind0]-x_tm[i])<0.0 )) // check for Time Model consystancy
        {
            x_tm.insert(i, x_d[ind0]);
            y_tm.insert(i, x_t[ind1]);
            xind_tm.insert(i, ind0);
            yind_tm.insert(i, ind1);
            n_tm++;
            recalculateData();
            tuning->tuneGraphic->graph(2)->data()->clear();
            tuning->tuneGraphic->graph(3)->data()->clear();}

    }

    if(QApplication::keyboardModifiers() & Qt::AltModifier)
    {

        bool selection2=false;

        for (int i = 0; i < n_tm; i++) {if (arrows[i]->selected()) {ind2=i; selection2=true;} }

        if (selection2){
            x_tm.remove(ind2);
            y_tm.remove(ind2);
            xind_tm.remove(ind2);
            yind_tm.remove(ind2);
            n_tm--;
            if (n_tm==0) {
                arrows.clear();
                depths.clear();
                for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows and depths
            }
            if (n_tm>0) recalculateData();

        }
    }


}

void MainWindow::on_pushButton_2_clicked()
{

    if (ui->label->text() == "Normal") {
        ui->label->setText("Reversed");
    } else {
         ui->label->setText("Normal");
    }

    ui->label->repaint();

    for (int i=0; i<n_d; ++i) y_d[i]=y_d[i]*(-1.0);
    replotAll();
    if (n_tm > 1) {recalculateData();
        correlation->corrGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
        correlation->corrGraphic->xAxis2->setRange(x_t[0], x_t[n_t-1]);
        correlation->corrGraphic->replot();
        tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
        tuning->tuneGraphic->xAxis2->setRange(x_t[0], x_t[n_t-1]);
        tuning->tuneGraphic->replot();
    }
}

void MainWindow::recalculateData()
{
    double r;

    if (n_d<2 || n_t<2) {QMessageBox msgBox;
        msgBox.setText("Download Data and Target first!");
        msgBox.exec();
        return;}

    if (n_tm == 0) {x_d1=x_d; return;}

    if (x_tm.first()<x_d.first() || x_tm.last()>x_d.last() || y_tm.first()<x_t.first() || y_tm.last()>x_t.last() ||
            x_tm.first()>x_d.last() || x_tm.last()<x_d.first() || y_tm.first()>x_t.last() || y_tm.last()<x_t.first()) {QMessageBox msgBox;
        msgBox.setText("Time Modes is inconsistent with Data or Target");
        msgBox.exec();
        return;}

    //tuning->tuneGraphic->graph(2)->data()->clear();
    arrows.clear();
    depths.clear();

    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows and depths



    if (n_tm==1) {arrows<< new QCPItemLine(tuning->tuneGraphic);
        arrows[0]->start->setAxes(tuning->tuneGraphic->xAxis2,tuning->tuneGraphic->yAxis2);
        arrows[0]->start->setCoords(x_d1[xind_tm[0]], y_dn[xind_tm[0]]);
        arrows[0]->end->setCoords(x_t[yind_tm[0]], y_tn[yind_tm[0]]);
        arrows[0]->setPen(Qt::DashDotLine);
        x_d1=x_d;
        return;
    }

    x_d1.resize(x_d.length());

    //for (int i=0; i<x_d.length(); ++i) mynr::linterp(x_tm, y_tm, x_d[i], x_d1[i]);
    mynr::linterpv(x_tm, y_tm, x_d, x_d1); // faster but x_d must be in increasing order...
    if (n_tm==2) {
        correlation->corrGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
        correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
        tuning->tuneGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
        tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
    }
    tuning->tuneGraphic->graph(0)->data()->clear();
    tuning->tuneGraphic->graph(0)->setData(x_d1, y_dn);
    tuning->tuneGraphic->graph(1)->data()->clear();
    tuning->tuneGraphic->graph(1)->setData(x_t, y_tn);

    correlation->corrGraphic->graph(0)->data()->clear();
    correlation->corrGraphic->graph(0)->setData(x_d1, y_d);
    correlation->corrGraphic->replot();

    sedrate->sedGraphic->graph(0)->data()->clear();
    for (int i=1; i<n_tm; i++) {

        sedrate->sedGraphic->graph(0)->addData(y_tm[i-1],(x_tm[i]-x_tm[i-1])/(y_tm[i]-y_tm[i-1]));
        sedrate->sedGraphic->graph(0)->addData(y_tm[i],(x_tm[i]-x_tm[i-1])/(y_tm[i]-y_tm[i-1]));
    }
    sedrate->sedGraphic->rescaleAxes();
    sedrate->sedGraphic->xAxis->setRange(tuning->tuneGraphic->xAxis->range().lower, tuning->tuneGraphic->xAxis->range().upper);
    sedrate->sedGraphic->replot();

    // plot arrows and depths labels
    for (int i=0; i < n_tm; i++) {//tuning->tuneGraphic->graph(2)->addData(y_tm[i], 0.0);
        arrows<< new QCPItemLine(tuning->tuneGraphic);
        arrows[i]->start->setCoords(x_d1[xind_tm[i]], y_dn[xind_tm[i]]);
        arrows[i]->end->setCoords(x_t[yind_tm[i]], y_tn[yind_tm[i]]);

        //  Depth
        depths<< new QCPItemText(tuning->tuneGraphic);

        depths[i]->position->setCoords(x_t[yind_tm[i]], 1.15); // place position at center/top of axis rect

        depths[i]->setText(QString::number(x_d[xind_tm[i]],'g',3));

    }

    // Correlation
    QVector<double> y_t1;
    mynr::linterpv(x_t, y_t, x_d1, y_t1);
    r = mynr::corr(y_d, y_t1);
    if (ui->checkBox_2->isChecked()) {ui->label_5->setText(QString::number(r,'g',3));} else {ui->label_5->setText(" - ");}
    tuning->tuneGraphic->replot();


}

void MainWindow::on_pushButton_4_clicked()
{
    correlation->show();
}

void MainWindow::on_pushButton_3_clicked()
{
    sedrate->show();
}

void MainWindow::on_actionSave_TimeModel_as_triggered()
{
    QString filename = QFileDialog::getSaveFileName( this, "Save file as:",fName);
    fName = filename;
    QFile f( filename );
    if (f.open( QIODevice::WriteOnly )){
        QTextStream stream(&f);
        for (int i=0; i<n_tm; i++){
            stream << x_tm[i] << " " << y_tm[i] << endl;

        }
        f.close();
    }
}

void MainWindow::on_actionSave_Data_with_ages_triggered()
{
    if (n_tm<2) {QMessageBox msgBox;
        msgBox.setText("Create at least 2 tie points");
        msgBox.exec();
        return;
    }

    QVector<double> x_d2;
    x_d2.resize(data_orig.x.length());
    mynr::linterpv(x_tm, y_tm, data_orig.x, x_d2);

    QString filename = QFileDialog::getSaveFileName( this, "Save file as:", fName);
    fName = filename;
    QFile f( filename );
    if (f.open( QIODevice::WriteOnly )){
        QTextStream stream(&f);
        stream <<  "Depth " << "Age "<< "Value" << endl;
        for (int i=0; i<data_orig.x.length(); i++){
            stream << data_orig.x[i] << " " << x_d2[i] << " " << data_orig.y[i] << endl;

        }
        f.close();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox msgBox;
            msgBox.setText("       QAnalySeries\n S. Kotov 2018-2020 (C)\n www.marum.de");
            msgBox.exec();
}

void MainWindow::on_actionOpen_Laskar_triggered()
{
    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows
    Laskar laskar;
    laskar.setModal(true);
    laskar.exec();
    n_t=y_t.length();

    if (n_t == 0) return;
    correlation->corrGraphic->graph(1)->data()->clear();
    correlation->corrGraphic->graph(1)->setData(x_t,y_t);
    correlation->corrGraphic->graph(1)->rescaleAxes();
    correlation->corrGraphic->setVisible(true);
    correlation->corrGraphic->replot();

    y_tn=y_t;// normalized target to [-1 0];
    double minv, maxv;

    minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
    maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());

    for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;};
    tuning->tuneGraphic->graph(1)->data()->clear();
    tuning->tuneGraphic->graph(1)->setData(x_t,y_tn);
    tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
    tuning->tuneGraphic->setVisible(true);
    tuning->tuneGraphic->replot();
    target_orig.x=x_t;
    target_orig.y=y_t;

}

void MainWindow::mouseWheel()
{
    QList<QCPAxis *> axlist;
    if(QApplication::keyboardModifiers() & Qt::ShiftModifier) {
        axlist << tuning->tuneGraphic->xAxis <<  tuning->tuneGraphic->xAxis2 << tuning->tuneGraphic->yAxis <<  tuning->tuneGraphic->yAxis2 ;}
    else {axlist.clear(); axlist << tuning->tuneGraphic->xAxis <<  tuning->tuneGraphic->xAxis2 ;}

    tuning->tuneGraphic->axisRect()->setRangeDragAxes(axlist);
    tuning->tuneGraphic->axisRect()->setRangeZoomAxes(axlist);

}

void MainWindow::on_actionOpen_Example_Dataset_triggered()
{
    x_d.clear();y_d.clear();x_d1.clear();y_dn.clear();
    x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;
    x_t.clear();y_t.clear();x_d1.clear();y_tn.clear();

    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows

    // Create sinusiod data
    double t,x;
    const double PI  = 3.141592653589793238463;
    t = 0.0;

    for (int i=0; i<1000; ++i)
    {
        x = sin(2*PI*t*10+7.0*sin(2*PI*t))*sin(3*PI*t);
        x_d.append(t); y_d.append(x);
        t = t+0.001;
    }



    n_d = x_d.length();
    x_d1=x_d;

    // Create target

    t = 0.0;

    for (int i=0; i<1000; ++i)
    {
        x = sin(2*PI*t*10);
        x_t.append(t+10); y_t.append(x);
        t = t+0.001;
    }
    n_t=y_t.length();
    data_orig.x=x_d;
    data_orig.y=y_d;
    target_orig.x=x_t;
    target_orig.y=y_t;

    replotAll();
}

void MainWindow::replotAll()
{
    double minv, maxv;
    if (n_d > 0) {
        minv = *std::min_element(y_d.constBegin(), y_d.constEnd());
        maxv = *std::max_element(y_d.constBegin(), y_d.constEnd());
        y_dn=y_d;// normalized data to [0 1];
        for (int i=0; i<n_d; i++) {y_dn[i]=(y_d[i]-minv)/(maxv-minv)+0.1;};
        //normalized = (x-min(x))/(max(x)-min(x));
        correlation->corrGraphic->graph(0)->data()->clear();
        correlation->corrGraphic->graph(0)->setData(x_d1,y_d);
        correlation->corrGraphic->graph(0)->rescaleAxes();
        correlation->corrGraphic->setVisible(true);
        correlation->corrGraphic->replot();

        tuning->tuneGraphic->graph(0)->data()->clear();
        tuning->tuneGraphic->graph(0)->setData(x_d1,y_dn);

        tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
        tuning->tuneGraphic->yAxis2->setRange(-1.2 , 1.2);
        tuning->tuneGraphic->setVisible(true);
        tuning->tuneGraphic->replot();

        sedrate->sedGraphic->rescaleAxes();
        sedrate->sedGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
        sedrate->sedGraphic->replot();
    }

    if (n_t > 0) {
        minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
        maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());
        y_tn=y_t;// normalized target to [-1 0];
        for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;};
        correlation->corrGraphic->graph(1)->data()->clear();
        correlation->corrGraphic->graph(1)->setData(x_t,y_t);
        correlation->corrGraphic->graph(1)->rescaleAxes();
        correlation->corrGraphic->setVisible(true);
        correlation->corrGraphic->replot();

        tuning->tuneGraphic->graph(1)->data()->clear();
        tuning->tuneGraphic->graph(1)->setData(x_t,y_tn);
        tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
        tuning->tuneGraphic->yAxis->setRange(-1.2 , 1.2);
        tuning->tuneGraphic->setVisible(true);
        tuning->tuneGraphic->replot();
    }

    sedrate->sedGraphic->graph(0)->data()->clear();
    for (int i=1; i<n_tm; i++) {

        sedrate->sedGraphic->graph(0)->addData(y_tm[i-1],(x_tm[i]-x_tm[i-1])/(y_tm[i]-y_tm[i-1]));
        sedrate->sedGraphic->graph(0)->addData(y_tm[i],(x_tm[i]-x_tm[i-1])/(y_tm[i]-y_tm[i-1]));
    }

}

void MainWindow::on_actionData_Processor_triggered()
{
    DataProc dataproc;
    dataproc.setModal(true);
    dataproc.exec();

    n_d = x_d.length();
    n_tm = 0;
    x_tm.clear(); y_tm.clear();
    depths.clear(); arrows.clear();
    recalculateData();
    replotAll();

    correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);

    tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
}

void MainWindow::on_actionSSA_triggered()
{
    SSA ssa;
    ssa.setModal(true);
    ssa.exec();

    n_d = x_d.length();
    n_t = x_t.length();
    n_tm = 0;
    x_tm.clear(); y_tm.clear();
    depths.clear(); arrows.clear();
    recalculateData();
    replotAll();

    correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);

    tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
}


void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::quit() ;
}

void MainWindow::on_actionOpen_project_triggered()
{
    if (command_line == 0) {

        fileName = QFileDialog::getOpenFileName(this,"Open Project");


        QFile file(fileName);
        if(!file.exists()){
            qDebug() << "File does not exist: "<<fileName;return;
        }else{
            qDebug() << fileName<<" opened";
        }
    }
    prName = fileName;
    double x,y;
    x_d.clear();y_d.clear();x_d1.clear();y_dn.clear();x_t.clear();y_t.clear();y_tn.clear();
    x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;

    std::ifstream infile(fileName.toStdString());
    infile >> n_d >> n_t >> n_tm;
    for (int i=0; i<n_d; ++i)
    {   infile >> x >> y;
        x_d.append(x); y_d.append(y);
    }
    for (int i=0; i<n_t; ++i)
    {   infile >> x >> y;
        x_t.append(x); y_t.append(y);
    }
    for (int i=0; i<n_tm; ++i)
    {   infile >> x >> y;
        x_tm.append(x); y_tm.append(y);
    }
    for (int i=0; i<n_tm; ++i)
    {   infile >> x >> y;
        xind_tm.append(x); yind_tm.append(y);
    }

    infile.close();

    double minv, maxv;
    minv = *std::min_element(y_d.constBegin(), y_d.constEnd());
    maxv = *std::max_element(y_d.constBegin(), y_d.constEnd());
    y_dn=y_d;// normalized data to [0 1];
    for (int i=0; i<n_d; i++) {y_dn[i]=(y_d[i]-minv)/(maxv-minv)+0.1;};
    minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
    maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());
    y_tn=y_t;// normalized target to [-1 0];
    for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;};

    recalculateData();
    replotAll();
    if (n_tm>=2) {
        tuning->tuneGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
        tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
        tuning->tuneGraphic->replot();
        correlation->corrGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
        correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
        correlation->corrGraphic->replot();
    }


}

void MainWindow::on_actionSave_project_as_triggered()
{
    QString filename = QFileDialog::getSaveFileName( this, "Save Project as...", prName);
    QFile f( filename );
    if (f.open( QIODevice::WriteOnly )){
        QTextStream stream(&f);

        prName = filename;
        stream << n_d << endl <<  n_t << endl << n_tm << endl ;
        for (int i=0; i<n_d; i++){
            stream << x_d[i] << " " << y_d[i] << endl;
        }
        for (int i=0; i<n_t; i++){
            stream << x_t[i] << " " << y_t[i] << endl;
        }
        for (int i=0; i<n_tm; i++){
            stream << x_tm[i] << " " << y_tm[i] << endl;
        }
        for (int i=0; i<n_tm; i++){
            stream << xind_tm[i] << " " << yind_tm[i] << endl;
        }

        f.close();
    }
}

void MainWindow::on_pushButton_5_clicked() // Reverse Data X axis
{
    if (ui->label_2->text() == "Normal") { ui->label_2->setText("Reversed");} else {ui->label_2->setText("Normal");}
    ui->label_2->repaint();

    for (int i=0; i<n_d; ++i) {x_d[i]=x_d[i]*(-1.0); x_d1[i]=x_d1[i]*(-1.0);}
    for (int i=0; i<floor(n_d/2.0);++i) {
        std::swap(x_d[i],x_d[n_d-1-i]);
        std::swap(x_d1[i],x_d1[n_d-1-i]);
        std::swap(y_d[i],y_d[n_d-1-i]);
    }
    n_tm = 0;
    x_tm.clear(); y_tm.clear();
    depths.clear(); arrows.clear();

    replotAll();

}

void MainWindow::on_pushButton_6_clicked()  // Reverse Target X axis
{
    if (ui->label_3->text() == "Normal") { ui->label_3->setText("Reversed");} else {ui->label_3->setText("Normal");}
    ui->label_3->repaint();

    for (int i=0; i<n_t; ++i) {x_t[i]=x_t[i]*(-1.0);}
    for (int i=0; i<floor(n_t/2.0);++i) {
        std::swap(x_t[i],x_t[n_t-1-i]);
        std::swap(y_t[i],y_t[n_t-1-i]);
    }
    n_tm = 0;
    x_tm.clear(); y_tm.clear();
    depths.clear(); arrows.clear();

    replotAll();
}

void MainWindow::on_pushButton_7_clicked()  // back to original
{
    ui->label->setText("Normal"); ui->label->repaint();
    ui->label_2->setText("Normal"); ui->label_2->repaint();
    ui->label_3->setText("Normal"); ui->label_3->repaint();
    x_d = data_orig.x;
    y_d = data_orig.y;
    x_t = target_orig.x;
    y_t = target_orig.y;
    x_tm = tm_orig.x;
    y_tm = tm_orig.y;
    xind_tm = xind_tm_orig;
    yind_tm = yind_tm_orig;
    n_d = x_d.length();
    n_t = x_t.length();
    n_tm = x_tm.length();
    recalculateData();
    replotAll();
    tuning->tuneGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
    tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
    tuning->tuneGraphic->replot();
    correlation->corrGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
    correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
    correlation->corrGraphic->replot();
}

void MainWindow::on_actionETP_Target_triggered()
{
    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows
    ETP etp;
    etp.setModal(true);
    etp.exec();

    if (n_t == 0) return;
    n_t=y_t.length();
    correlation->corrGraphic->graph(1)->data()->clear();
    correlation->corrGraphic->graph(1)->setData(x_t,y_t);
    correlation->corrGraphic->graph(1)->rescaleAxes();
    correlation->corrGraphic->setVisible(true);
    correlation->corrGraphic->replot();

    y_tn=y_t;// normalized target to [-1 0];
    double minv, maxv;

    minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
    maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());

    for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;};
    tuning->tuneGraphic->graph(1)->data()->clear();
    tuning->tuneGraphic->graph(1)->setData(x_t,y_tn);
    tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
    tuning->tuneGraphic->setVisible(true);
    tuning->tuneGraphic->replot();
    target_orig.x=x_t;
    target_orig.y=y_t;
}

void MainWindow::onSedrateDrop()
{
    recalculateData();
    tuning->tuneGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
    tuning->tuneGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
    tuning->tuneGraphic->replot();
    correlation->corrGraphic->xAxis->setRange(x_d1[0], x_d1[n_d-1]);
    correlation->corrGraphic->xAxis2->setRange(x_d1[0], x_d1[n_d-1]);
    correlation->corrGraphic->replot();
}

void MainWindow::on_actionSave_new_Data_as_triggered()
{
    QString filename = QFileDialog::getSaveFileName( this, "Save file as:",fName);
    fName = filename;
    QFile f( filename );
    if (f.open( QIODevice::WriteOnly )){
        QTextStream stream(&f);
        for (int i=0; i<n_d; i++){
            stream << x_d[i] << " " << y_d[i] << endl;

        }
        f.close();
    }
}

void MainWindow::on_actionSave_new_Target_as_triggered()
{
    QString filename = QFileDialog::getSaveFileName( this, "Save file as:",fName);
    fName = filename;
    QFile f( filename );
    if (f.open( QIODevice::WriteOnly )){
        QTextStream stream(&f);
        for (int i=0; i<n_t; i++){
            stream << x_t[i] << " " << y_t[i] << endl;

        }
        f.close();
    }
}

void MainWindow::on_pushButton_8_clicked() // Reverse Y target
{
    if (ui->label_4->text() == "Normal") {
        ui->label_4->setText("Reversed");
    } else {
         ui->label_4->setText("Normal");
    }

    ui->label_4->repaint();

    for (int i=0; i<n_t; ++i) y_t[i]=y_t[i]*(-1.0);
    replotAll();
    if (n_tm > 1) {recalculateData();
        correlation->corrGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
        correlation->corrGraphic->xAxis2->setRange(x_t[0], x_t[n_t-1]);
        correlation->corrGraphic->replot();
        tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
        tuning->tuneGraphic->xAxis2->setRange(x_t[0], x_t[n_t-1]);
        tuning->tuneGraphic->replot();
    }
}

void MainWindow::on_actionInsolation_La04_triggered()
{
    arrows.clear();
    depths.clear();
    for (int i = tuning->tuneGraphic->itemCount()-1; i>=0 ; i--) tuning->tuneGraphic->removeItem(i); // clear arrows
    Insolation insolation;
    insolation.setModal(true);
    insolation.exec();
    n_t=y_t.length();
    if (n_t == 0) return;

    correlation->corrGraphic->graph(1)->data()->clear();
    correlation->corrGraphic->graph(1)->setData(x_t,y_t);
    correlation->corrGraphic->graph(1)->rescaleAxes();
    correlation->corrGraphic->setVisible(true);
    correlation->corrGraphic->replot();

    y_tn=y_t;// normalized target to [-1 0];
    double minv, maxv;

    minv = *std::min_element(y_t.constBegin(), y_t.constEnd());
    maxv = *std::max_element(y_t.constBegin(), y_t.constEnd());

    for (int i=0; i<n_t; i++) {y_tn[i]=(y_t[i]-minv)/(maxv-minv)-1.1;};
    tuning->tuneGraphic->graph(1)->data()->clear();
    tuning->tuneGraphic->graph(1)->setData(x_t,y_tn);
    tuning->tuneGraphic->xAxis->setRange(x_t[0], x_t[n_t-1]);
    tuning->tuneGraphic->setVisible(true);
    tuning->tuneGraphic->replot();
    target_orig.x=x_t;
    target_orig.y=y_t;

}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if (ui->checkBox->isChecked()) {tuning->tuneGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
        tuning->tuneGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
        correlation->corrGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 5));
        correlation->corrGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 5));

    }
    else {tuning->tuneGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot));
        tuning->tuneGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot));
        correlation->corrGraphic->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot));
        correlation->corrGraphic->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot));

    }
    tuning->tuneGraphic->replot();
    correlation->corrGraphic->replot();

}

void MainWindow::on_checkBox_2_stateChanged(int arg1)
{
    recalculateData(); //replotAll();
}

void readFile() // fType: d (data), t (target), or tm (time model)
{
//    QRegularExpression separator("(\\s*,\\s*|\\s*;\\s*|\\s+)");
//    QFile inputFile(fileName);

    Readcsv readcsv;
    readcsv.setModal(true);
    readcsv.exec();


    /*if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        QString line = in.readAll();
        line = line.trimmed();
        QStringList list = line.split(separator);


        for (int i = 0; i<list.length();i+=2)
        {
            x.append(list.at(i).toDouble());
            y.append(list.at(i+1).toDouble());
            if (i>0) if (x[x.length()-1]<=x[x.length()-2]) {QMessageBox msgBox;
                msgBox.setText("x must be in increasing order!");
                msgBox.exec();
                return;}

        }
        inputFile.close();
    }
    if (fType == "d") {
        x_d.clear();y_d.clear();x_d1.clear();y_dn.clear();
        x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;

        x_d = x;
        y_d = y;
        n_d=y_d.length();
        x_d1=x_d;
        data_orig.x=x_d;
        data_orig.y=y_d;


    }
    if (fType == "t") {
        x_t.clear();y_t.clear();y_tn.clear();
        x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();n_tm=0;
        x_t = x;
        y_t = y;
        n_t=y_t.length();
        target_orig.x=x_t;
        target_orig.y=y_t;
    }
    if (fType == "tm") {

    }
*/
}
