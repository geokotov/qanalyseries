#ifndef READCSV_H
#define READCSV_H

#include <QDialog>

namespace Ui {
class Readcsv;
}

class Readcsv : public QDialog
{
    Q_OBJECT

public:
    explicit Readcsv(QWidget *parent = nullptr);

    ~Readcsv();

private slots:

    void on_buttonBox_accepted();

private:
    Ui::Readcsv *ui;
    bool str_ok = true;

};

#endif // READCSV_H
