#-------------------------------------------------
#
# Project created by QtCreator 2017-05-18T10:55:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = QAnalySeries
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    globals.cpp \
    correlation.cpp \
    readcsv.cpp \
    sedrate.cpp \
    tuning.cpp \
    mynr.cpp \
    laskar.cpp \
    dataproc.cpp \
    ssa.cpp \
    etp.cpp \
    insolation.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    globals.h \
    correlation.h \
    readcsv.h \
    sedrate.h \
    tuning.h \
    main.h \
    mynr.h \
    laskar.h \
    dataproc.h \
    ssa.h \
    etp.h \
    insolation.h

FORMS    += mainwindow.ui \
    correlation.ui \
    readcsv.ui \
    sedrate.ui \
    tuning.ui \
    laskar.ui \
    dataproc.ui \
    ssa.ui \
    etp.ui \
    insolation.ui

INCLUDEPATH += ../eigen337

DISTFILES +=
