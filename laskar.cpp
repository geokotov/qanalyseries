#include "laskar.h"
#include "ui_laskar.h"
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include "QDebug"
#include "QSqlQueryModel"
#include "QVector"
#include "QFile"
#include "globals.h"
#include <QMessageBox>
#include "QDir"

Laskar::Laskar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Laskar)
{
    ui->setupUi(this);
    QDir::setCurrent(QCoreApplication::applicationDirPath());

    QString OS = QSysInfo::productType();
    QString pathToResources;
    if (OS=="osx") pathToResources="..//Resources/"; else pathToResources="";


    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(pathToResources+"laskar.db3");
    db.open();

    QSqlQuery query;
    query.exec("SELECT * FROM sqlite_master WHERE type='table'");

    while (query.next())
    {

        ui->comboBox->addItem(query.value("tbl_name").toString());
    }

    query.clear();
    query.exec("SELECT min(t) FROM " + ui->comboBox->itemText(0));
    query.first();
    ui->lineEdit->setText(query.value(0).toString());
    query.clear();
    query.exec("SELECT max(t) FROM " + ui->comboBox->itemText(0));
    query.first();
    ui->lineEdit_2->setText(query.value(0).toString());

}

Laskar::~Laskar()
{
    delete ui;
}


void Laskar::on_pushButton_clicked()
{
    QDir::setCurrent(QDir::homePath());
    Laskar::close();

}

void Laskar::on_pushButton_2_clicked()
{
    x_tm.clear(); y_tm.clear(); xind_tm.clear(); yind_tm.clear();
    bool ok;
    ui->lineEdit->text().toDouble(&ok);
    if (!ok) {QMessageBox msgBox;
        msgBox.setText("Invalid number format");
        msgBox.exec();
        return;}
    ui->lineEdit_2->text().toDouble(&ok);
    if (!ok) {QMessageBox msgBox;
        msgBox.setText("Invalid number format");
        msgBox.exec();
        return;}

    QSqlQuery query;
    query.exec("SELECT t, s FROM "+ ui->comboBox->currentText()+" where t between "+ui->lineEdit->text()+" and "+ui->lineEdit_2->text());

    x_t.clear();y_t.clear(); n_t=0; n_tm=0;
    while (query.next())
    {
        x_t.append(query.value("t").toDouble());
        y_t.append(query.value("s").toDouble());
    }
    QDir::setCurrent(QDir::homePath());
    Laskar::close();
}

void Laskar::on_comboBox_currentIndexChanged(const QString &arg1)
{
    QSqlQuery query;
    query.clear();
    query.exec("SELECT min(t) FROM " + ui->comboBox->currentText());
    query.first();
    ui->lineEdit->setText(query.value(0).toString());
    query.clear();
    query.exec("SELECT max(t) FROM " + ui->comboBox->currentText());
    query.first();
    ui->lineEdit_2->setText(query.value(0).toString());
}
