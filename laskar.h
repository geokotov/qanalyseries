#ifndef LASKAR_H
#define LASKAR_H

#include <QDialog>

namespace Ui {
class Laskar;
}

class Laskar : public QDialog
{
    Q_OBJECT

public:
    explicit Laskar(QWidget *parent = 0);
    ~Laskar();



private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::Laskar *ui;
};

#endif // LASKAR_H
