#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"qcustomplot.h"
#include "correlation.h"
#include "sedrate.h"
#include "tuning.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
     ~MainWindow();


   void recalculateData();
   void replotAll();


private slots:

    void on_pushButton_clicked();

    void on_actionOpen_Data_triggered();

    void on_actionOpen_Target_triggered();

    void on_actionOpen_TimeModel_triggered();

    void mousePress();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_actionSave_TimeModel_as_triggered();

    void on_actionSave_Data_with_ages_triggered();

    void on_actionAbout_triggered();

    void on_actionOpen_Laskar_triggered();

    void mouseWheel();

    void on_actionOpen_Example_Dataset_triggered();

    void on_actionData_Processor_triggered();

    void on_actionSSA_triggered();

    void on_actionExit_triggered();

    void on_actionOpen_project_triggered();

    void on_actionSave_project_as_triggered();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_actionETP_Target_triggered();

    void onSedrateDrop();


    void on_actionSave_new_Data_as_triggered();

    void on_actionSave_new_Target_as_triggered();

    void on_pushButton_8_clicked();

    void on_actionInsolation_La04_triggered();

    void on_checkBox_stateChanged(int arg1);

    void on_checkBox_2_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    Correlation *correlation;
    Sedrate *sedrate;
    Tuning *tuning;
};

void readFile();

#endif // MAINWINDOW_H
